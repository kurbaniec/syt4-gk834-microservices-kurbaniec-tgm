# Windzentrale V2
Basiert auf der Mircroservices-Demo ([Link](https://github.com/paulc4/microservices-demo))
und der alten Windzentrale ([Link](https://github.com/TGM-HIT/syt4-gk831-document-middleware-mongodb-kurbaniec-tgm)).

## Starten
Zum Starten würde ich eine IDE mit Spring-Plugin empfehlen, um die drei Services (Registration, User, Data) bequem zu starten.
