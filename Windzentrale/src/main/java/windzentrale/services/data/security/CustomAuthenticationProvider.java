package windzentrale.services.data.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import windzentrale.services.data.DataUserService;

import java.util.ArrayList;

/**
 * Custom authentication in order to use the the method
 * {@link windzentrale.services.user.UserController#authentication(String, String)} from
 * our User-Service for authentication.
 *
 * @author Kacper Urbaniec
 * @version 2019-04-15
 */

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    DataUserService userService;

    /**
     * Checks if user input matches a user-account via User-Service.
     *
     * @throws BadCredentialsException when username and password do not match.
     */
    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        // use the credentials
        // and authenticate against the third-party system
        if (userService.authenticate(name, password)) {
            return new UsernamePasswordAuthenticationToken(
                    name, password, new ArrayList<>());
        } else {
            throw new BadCredentialsException("Invalid username and password");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
                UsernamePasswordAuthenticationToken.class);
    }
}