package windzentrale.services.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import windzentrale.exceptions.AccountNotFoundException;
import windzentrale.services.data.model.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.*;
import java.util.logging.Logger;

/**
 * A RESTFul controller for accessing windpark data information.
 *
 * @author Kacper Urbaniec
 * @version 2019-04-15
 */
@RestController
@Component
public class DataController {
	@Autowired
	WindparkDataRepository repository;

	@Value("${windpark.count}")
	private String windCountString;

	protected Logger logger = Logger.getLogger(DataController.class
			.getName());

	@Autowired
	protected DataUserService userService;

	public DataController(DataUserService userService) {
		this.userService = userService;
	}

	// Gespeicherte Daten als XML ausgeben
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/windzentrale/data_xml", produces = MediaType.APPLICATION_XML_VALUE)
	public WindzentraleData windparkDataXML() {
		return dataBuilder();
	}

	// Gespeicherte Daten als XML ausgeben | Alternative
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/windzentrale/data_xml2", produces = MediaType.APPLICATION_XML_VALUE)
	public String windparkDataXML2() {
		return dataBuilderXML();
	}


	// Gespeicherte Daten als JSON ausgeben
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/windzentrale/data_json", produces = MediaType.APPLICATION_JSON_VALUE)
	public WindzentraleData windparkDataJSON() {
		return dataBuilder();
	}

	private WindzentraleData dataBuilder() {
		HashMap<String, ArrayList<WindengineData>> zentrale = new HashMap<>();
		int windcount = Integer.parseInt(windCountString);
		for(int i = 0; i < windcount; i++) {
			List<WindparkData> wp = repository.findByWindparkID(""+i);
			ArrayList<WindengineData> we = new ArrayList<>();
			for(WindparkData data: wp) {
				we.addAll(Arrays.asList(data.getWindpark()));
			}
			Collections.sort(we);
			zentrale.put(""+i, we);
		}
		return new WindzentraleData(zentrale);
	}

	private String dataBuilderXML() {
		WindzentraleData zentrale = dataBuilder();
		String xml = "";
		try {
			StringWriter sw = new StringWriter();
			JAXBContext jaxbContext = JAXBContext.newInstance(WindzentraleData.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.marshal(zentrale, sw);
			xml = sw.toString();
			System.out.println(xml);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return xml;
	}

	// Used for testing the authentication process.
	/**
	 @RequestMapping("/test")
	 public boolean test() {
	 return userService.authenticate("123456789", "1234");
	 }

	 @RequestMapping("/test2")
	 public List<WindengineData> test2() {
	 return repository.findEngineDataByWindpark("0", "0");
	 }*/
}
