package windzentrale.services.data.web;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

public class SearchCriteria {
	private String windparkField;

	private String windengineField;

	public String getWindparkField() {
		return windparkField;
	}

	public void setWindparkField(String windparkField) {
		this.windparkField = windparkField;
	}

	public String getWindengineField() {
		return windengineField;
	}

	public void setWindengineField(String windengineField) {
		this.windengineField = windengineField;
	}

	public boolean validate(Errors errors) {
		if (windparkField.length() > 3)
			errors.rejectValue("windparkField", "badFormat",
					"WindparkID should be no longer than 3 digits");
		else if (StringUtils.hasText(windparkField)) {
			try {
				Integer.parseInt(windparkField);
			} catch (NumberFormatException e) {
				errors.rejectValue("windparkField", "badFormat",
						"WindparkID should be digits");
			}
		}
		if (windengineField.length() > 3)
			errors.rejectValue("windengineField", "badFormat",
					"WindengineID should be no longer than 3 digits");
		else if (StringUtils.hasText(windengineField)) {
			try {
				Integer.parseInt(windengineField);
			} catch (NumberFormatException e) {
				errors.rejectValue("windengineField", "badFormat",
						"WindengineID should be digits");
			}
		}
		if (StringUtils.hasText(windengineField) && !StringUtils.hasText(windparkField)) {
			errors.rejectValue("windparkField", "badFormat",
					"Windpark must be specified when looking for Windengine");
		}

		return errors.hasErrors();
	}

	@Override
	public String toString() {
		return (StringUtils.hasText(windparkField) ? "WindparkID: " + windparkField
				: "")
				+ (StringUtils.hasText(windengineField) ? " WindengineID: " + windengineField
						: "");
	}
}
