package windzentrale.services.data.model;

import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import java.util.*;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

/**
 * Implements {@link WindparkDataQueries} to allow more complex queries on the MongoRepository
 * {@link WindparkDataRepository}.
 *
 * @author Kacper Urbaniec
 * @version 2019-04-16
 */

@Component
public class WindparkDataRepositoryImpl implements WindparkDataQueries {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    WindparkDataRepository repository;

    @Value("${windpark.count}")
    private String windCountString;

    /**
     * Searches after WindengineData of a specific Windengine on a specific Windpark.
     */
    @Override
    public List<WindengineData> findEngineDataByWindpark(String windengineID, String windparkID) {
        /*
           Based on following MongoDB-aggregation:
           [{"$match": {"$and": [{"windparkID": "0"}, {"data": {"$elemMatch": {"windengineID": "0"}}}]}},
           { "$unwind": "$data"},
           { "$match": {"data.windengineID": "0"}}]
        */
        Aggregation agg = newAggregation(
            Aggregation.match(new Criteria().andOperator(Criteria.where("windparkID").is(windparkID),
                    Criteria.where("data").elemMatch(Criteria.where("windengineID").is(windengineID)))),
            Aggregation.unwind("data"),
            Aggregation.match(Criteria.where("data.windengineID").is("0")));

        AggregationResults<WindengineData> results =
                mongoTemplate.aggregate(agg, "windparkData", WindengineData.class);

        Document allDocuments = results.getRawResults();
        ArrayList<Document> rawData = (ArrayList<Document>) allDocuments.get("result");

        // Handle conversion JSON -> WindengineData
        Gson gson = new Gson();

        ArrayList<WindengineData> resultSet = new ArrayList<>();

        for (Document data : rawData) {
            // Get WindengineData as BSON
            Document rawOne = (Document) data.get("data");
            // Convert BSON -> JSON -> Java Object
            // Add result
            resultSet.add(gson.fromJson(rawOne.toJson(), WindengineData.class));

        }
        Collections.sort(resultSet);
        return resultSet;
    }

    /**
     * Searches for all data from a windpark.
     * <br>
     * @return Findings formatted as {@link WindzentraleData}.
     */
    @Override
    public WindzentraleData findDataByWindpark(String windparkID) {
        HashMap<String, ArrayList<WindengineData>> resultSet = new HashMap<>();
        List<WindparkData> wp = repository.findByWindparkID(windparkID);
        ArrayList<WindengineData> we = new ArrayList<>();
        for(WindparkData data: wp) {
            we.addAll(Arrays.asList(data.getWindpark()));
        }
        Collections.sort(we);
        resultSet.put(windparkID, we);
        if (!resultSet.isEmpty()) {
            return new WindzentraleData(resultSet);
        }
        else return null;
    }

    /**
     * Searches for all data from all windparks.
     * <br>
     * @return Findings formatted as {@link WindzentraleData}.
     */
    @Override
    public WindzentraleData findAllData() {
        HashMap<String, ArrayList<WindengineData>> resultSet = new HashMap<>();
        int windcount = Integer.parseInt(windCountString);
        for(int i = 0; i < windcount; i++) {
            List<WindparkData> wp = repository.findByWindparkID(""+i);
            ArrayList<WindengineData> we = new ArrayList<>();
            for(WindparkData data: wp) {
                we.addAll(Arrays.asList(data.getWindpark()));
            }
            Collections.sort(we);
            resultSet.put(""+i, we);
        }
        if (!resultSet.isEmpty()) {
            return new WindzentraleData(resultSet);
        }
        else return null;
    }


}
