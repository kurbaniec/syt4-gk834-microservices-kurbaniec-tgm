package windzentrale.services.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

/**
 * Hide the access to the microservice inside this local service.
 * 
 * @author Kacper Urbaniec
 * @version 2019-04-15
 */
@Service
public class DataUserService {

	@Autowired
	@LoadBalanced
	protected RestTemplate restTemplate;

	public static final String serviceUrl = "http://USER-SERVICE";

	protected Logger logger = Logger.getLogger(DataUserService.class
			.getName());


	/**
	 * Sends a username and password to the User-Service, to see if it is a valid account.
	 *
	 * @return True, when username exist and password matches, else false.
	 * @throws AuthenticationServiceException when the User-Service is not available.
	 */
	public boolean authenticate(String username, String password) throws AuthenticationServiceException {
		logger.info("Sending authentication request for user " + username + " to User-Service");
		if (username.equals("") || password.equals("")) {
			throw new BadCredentialsException("Invalid username and password");
		}
		try {
			Boolean result = restTemplate.postForObject(serviceUrl + "/auth/user/{name}/password/{passwd}", "",
					Boolean.class, username, password);
			if (result) {
				logger.info("Authentication successful");
			}
			else logger.info("Authentication failed");
			return result;
		}
		catch (Exception ex) {
			logger.warning("Authentication failed because " + ex.getMessage());
			throw new AuthenticationServiceException("User-Service not available");
		}
	}

}
