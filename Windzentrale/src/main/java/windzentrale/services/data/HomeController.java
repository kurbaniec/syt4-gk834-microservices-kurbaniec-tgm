package windzentrale.services.data;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Home page controller.
 * 
 * @author Kacper Urbaniec
 * @version 2019-04-17
 */
@Controller
public class HomeController {
	
	@RequestMapping("/")
	public String home() {
		return "index";
	}

	@RequestMapping("/data_table")
	public String dataTable() {
		return "dataTable";
	}

	@RequestMapping("/login")
	public String login() {
		return "login";
	}

}
