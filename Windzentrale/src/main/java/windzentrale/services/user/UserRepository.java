package windzentrale.services.user;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Used for querying MongoDB for the user collection.
 * @author Kacper Urbaniec
 * @version 2019-04-15
 */

public interface UserRepository extends MongoRepository<User, String> {
    /**
     * Find an account with the specified account number.
     *
     * @param accountNumber
     * @return The account if found, null otherwise.
     */
    public User findByNumber(String accountNumber);


    /**
     * Find user whose owner name contains the specified string
     *
     * @param partialName
     *            Any alphabetic string.
     * @return The list of matching user - always non-null, but may be
     *         empty.
     */
    public List<User> findByOwnerContainingIgnoreCase(String partialName);

    /**
     * Find all data in collection.
     *
     * @return Full collection in a list.
     */
    @Query("{}")
    public List<User> findAll();

    /**
     * Fetch the number of user known to the system.
     *
     * @return The number of user.
     */
    public default int countAll() {
        return this.findAll().size();
    }

}
