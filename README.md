# Middleware Engineering "Microservices"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

Zu aller erst habe ich das Beispiel-Repository, was in der Microservice Einführung von Spring verwendet wird, geholt. Es ist unter folgenden [Link](<https://github.com/paulc4/microservices-demo>) zu finden. 

Dann habe ich alle drei Services zum Testen per IDE gestartet, um zu sehen ob alles funktioniert.

![Run Config](img/RunConfig.PNG)

*Run Config* 

### Registration Server

Zu aller erst muss der **Registration Server** gestartet werden. Dieser erfüllt den Zweck, dass sich die einzelnen Microservices finden. Somit ist dieser vergleichbar mit einem RMI-Registry. Er basiert auf den **Eureka**-Server, den Netflix für ihre Dienste entwickelt hat, um das "Microservice-Gegenseitig-Finden"-Problem zu lösen. Der Discovery-Server **Eureka** wurde Open-Source und seitdem in Spring Cloud eingegliedert.

```Java
@SpringBootApplication
@EnableEurekaServer
public class RegistrationServer {

	public static void main(String[] args) {
		// Tell server to look for registration.properties or registration.yml
		System.setProperty("spring.config.name", "registration-server");

		SpringApplication.run(RegistrationServer.class, args);
	}
}
```

Alle Services im Beispiel Projekt verfügen über eigene Config-Files:

```yaml
# Ignore JDBC Dependency
# This demo puts 3 applicatons in the same project so they all pick up the
# JDBC Depdendency, but this application doesn't need it.
spring.autoconfigure.exclude: org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration

# Configure this Discovery Server
eureka:
  instance:
    hostname: localhost
  client:  # Not a client, don't register with yourself
    registerWithEureka: false
    fetchRegistry: false

server:
  port: 1111   # HTTP (Tomcat) port

# Discovery Server Dashboard uses FreeMarker.  Don't want Thymeleaf templates
spring:
  thymeleaf:
    enabled: false     # Disable Thymeleaf 
  datasource:
    type: org.springframework.jdbc.datasource.SimpleDriverDataSource
```

*registration-server.yml*

Die Config-Files dienen zur Konfiguration dieser selbst und für das Zusammenspiel mit dem Eureka-Server. Da der Registration-Server der Eureka-Server selbst ist, sollte er sich selbst nicht registrieren.

Wenn man die Seite des Registration-Servers ([http://localhost:1111](http://localhost:1111/)) öffnet und die beiden anderen Dienste am Laufen hat, sollte man über folgendes Eureka-Dashboard verfügen.

![Eureka Dashboard](img/RegistrationServer.PNG)

*Eureka-Dashboard*

### Account Server

Das Beispiel besitzt einen Microservice für Konten, die aus einer lokalen H2-Datenbank ausgelesen werden und per REST-Schnittstelle verfügbar gemacht werden. Die Datenbank ist unter `Example_Code/src/main/resources/testdb` zu finden. 

Wie der Registration-Server verfügt sie über ein Config-File `accounts-server.yml`, über den der Server-Port festgelegt wird und das Einbinden in Eureka geschieht. 

Für die REST-Schnittstelle wird ein REST-Controller verwendet, der per URL + Account-ID (`/accounts/{accountNumber}`) die Account-Daten aus der Datenbank per Repository rausfiltert und ausgibt.

Ich habe in der `data.sql` angeschaut, welche Daten eingefügt werden und diese am Server per Account-Number  (<http://localhost:2222/accounts/123456789>) gesucht:

![Search](/img/AccountServer.PNG)

*Gefundener Account aus REST-Schnittstelle*

### Web Server

Der Webserver dient zur Finden und Visualieren der Daten, die vom Account-Service angeboten werden. Wie schon die Services vorher, besitzt auch dieser wieder sein eigenes Config-File.

Neu ist, dass er wirklich für die Webanzeige optimiert ist. Der Webserver besitzt mehrere html-Seiten als Templates im `resources`-Folder und mehre Controller. Außerdem werden häufig Anfragen an den Account-Controller gesendet. 

Die html-Seiten benutzen die REST-Schnittstellen vom Account-Service zum Holen von Konto-Daten, die dann per Thymeleaf dynamisch ins Template geladen werden. 

Zum Beispiel bei der Suche eines Konto durch die ID wird zuerst die ID-Nummer auf Gültigkeit im `WebAccountController` durch die Klasse `SearchCriteria` überprüft. Wenn diese gültig ist werden die Account-Daten im `WebAccountService`  durch die Methode `findByNumber` per REST geholt und in der Methode `byNumber` wieder im Controller dynamisch mit einer html-Seite `account` zurückgegeben.  

Dies sieht dann so aus:

![Web Search](img/WebServerBefore.PNG)

![Web Match](img/WebServer.PNG)

---

### Einbindung von User Service

Zuerst habe ich in meiner MongoDB-Datenbank eine neue Collection für User erstellt. Diese haben ich dann mit den Testdaten aus der H2-Datenbank gefüllt, diese mussten zuerst auf eine JSON-Notation gewandelt werden:

```json
[
  {
    "number": "123456789",
    "owner": "Keri Lee",
    "passwd": "1234"
  },
  {
    "number": "123456001",
    "owner": "Dollie R. Schnnumbert",
    "passwd": "1234"
  },
  ...
```

*data.json*

Grundsätzlich habe ich den Example-Code der Microservice-Demo übernommen, nur an die Beispiel-Bedürfnisse angepasst und meinen bestehenden Code aus der Document-Middleware Übung hinzugefügt. 

Aus dem Account-Service wurde der User-Service. Die größte Hürde war der Datenbankwechsel, also von H2 auf MongoDB. Ich erstellte ein zusätzliches Repository, was *MongoRepository* extended. 

In den Dependencies habe ich H2 rauskommentiert und stattdessen die Mongo-eigenen hinzugefügt. Im Config-File des User-Services musste ich dann die Mongo-Parameter angeben:

```yaml
# Spring properties
spring:
  application:
     name: user-service  # Service registers under this name
  data:
    mongodb.database: test
    mongodb.host: localhost
    mongodb.port: 27017
```

*user-server.yml*

Bei den Queries hatte ich das Problem, dass keine *findAll*- oder *countAll*-Queries standardmäßig existieren, nur welche wo zusätzliche Parameter gefragt werden. Also definierte ich diese selber:

```java
@Query("{}")
public List<User> findAll();

public default int countAll() {
	return this.findAll().size();
}
```

*UserRepository*

Der Rest des Codes musste nur leicht adaptiert werden, wobei die Konfigurations-Klasse `UserConfiguration` nicht mehr vonnöten ist, da die H2-Datenbank nicht mehr verwendet wird. 

Ich hatte kurze Zeit den Fehler, dass meine Queries keine Resultate lieferten. Das Problem war, dass ich bei der Grundklasse `User` den Fehler gemacht hatte, die Parameter ohne `@XmlElement` zu annotieren. Nach der Ausbesserung funktionierten die Queries problemlos.

Ich habe ein bisschen die html-Seiten überarbeitet, aber grundsätzlich funktioniert der User-Service fast genauso wie der Account-Service von der Oberfläche aus betrachtet:

![User Search](img/UserServerBefore.PNG)

![User Match](img/UserServer.PNG)

### Einbindung von Data Service

Als Basis nahm für den Data Service nahm ich den gerade erstellen User-Service, benutzte aber auch Element des aus der Demo vorhanden Web-Services. 

Anfangs wollte ich den Web-Service als alleinige Grundlage nehmen, dieser ist aber aus mir nicht bekannten Gründen ziemlich statisch, da der ComponentScan (`@ComponentScan(useDefaultFilters = false) // Disable component scanner`) ausgeschaltet ist und alles manuell per Beans aufgebaut ist. Für den Data-Service konnte ich sowas einfach nicht verwenden, vor allem weil mein Quellcode aus der vorherigen Übung für das Holen der Daten von anderen Windparks, welchen ich hier wieder verwenden möchte auf einem Spring-Service basiert und dieser funktioniert ohne ComponentScan einfach nicht.

Zu aller erst nahm ich mir die Authentifizierung via User-Service vor. Als Basis dazu nahm ich Spring-Security, mit dem ich schon vorher gearbeitet habe. Spring-Security schützt festgelegte URLs mit einem Login und weiteren Maßnahmen. Was bisschen problematisch war, als ich die Spring-Security Dependencies hinzugefügt habe, wurden alle drei Service dadurch gesichert waren. Dies kann ungeahnte Einschränkungen besitzen, also musste ich Spring-Security beim Registration- und User-Service ausschalten. Dies kann im Config-File gemacht werden:

```yaml
# Spring properties
spring:
  ...
  security:
    ignored: /**
  autoconfigure:
    exclude: org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
```

*user-server.yml*

Für die Authentifizierung selber benutze ich die REST-Schnittstelle vom User-Service. 

```java
/**
 * Sends a username and password to the User-Service, to see if it is a valid account.
 *
 * @return True, when username exist and password matches, else false.
 * @throws AuthenticationServiceException when the User-Service is not available.
 */
public boolean authenticate(String username, String password) throws AuthenticationServiceException {
	logger.info("Sending authentication request for user " + username + " to User-Service");
	if (username.equals("") || password.equals("")) {
		throw new BadCredentialsException("Invalid username and password");
	}
	try {
		Boolean result = restTemplate.postForObject(serviceUrl + "/auth/user/{name}/password/{passwd}", "", Boolean.class, username, password);
		if (result) {
			logger.info("Authentication successful");
		}
		else logger.info("Authentication failed");
		return result;
	}
	catch (Exception ex) {
		logger.warning("Authentication failed because " + ex.getMessage());
		throw new AuthenticationServiceException("User-Service not available");
	}
}
```

*Aus Klasse `DataUserService` - Data Service*

Ich sende eine Anfrage der Form `/auth/user/{name}/password/{passwd}` an ihn, auf die der User-Service mit Ja, falls Username und Passwort übereinstimmen, sonst Nein antwortet. Als ich bei der Übermittlungsmethode von GET auf POST wechselte, funktionierte die Übertragung nicht mehr. 

In einem Forum fand ich einen netten Workaround, man gibt im RequestMapping beim User-Service nur `produces="application/json"` hinzu. 

```java
@RequestMapping(value = "/auth/user/{name}/password/{passwd}",
			method = RequestMethod.POST, produces="application/json")
public boolean authentication(@PathVariable("name") String username, @PathVariable("passwd") String password) {
	logger.info("Authentication for User: " + username);
	try {
		User user = userRepository.findByNumber(username);
		return (user.getPasswd().equals(password));
	}
	catch (Exception ex) {
		logger.warning("Authentication Error: " + ex.getMessage());
	}
	return false;
}
```

*Aus Klasse `UserController` - User Service*

Jetzt funktioniert die Authentifikation wieder:

![Authentication Curl](img/curljson.PNG)

*Verifikation via URL*

Jetzt musste ich meine Authentifizierung via REST mit Spring-Security verbinden. Dazu musste ich einen eigenen `AuthenticationProvider` erstellen, der statt dem default-mäßigen eingesetzt werden sollte. In meiner `CustomAuthenticationProvider`-Klasse bekommt der Authentifikator in der Methode `authenticate` Usernamen und Password (die von Login-Seite duch Spring geroutet werden) und überprüft sie durch meinen `DataUserService`. Dieser macht nichts anderes als den Curl-Befehl von vorher, nur jetzt als Java-Code. 

Wenn der User-Service bei der Authentifizierung nicht vorhanden ist, sprich offline ist, wird eine `AuthenticationServiceException` geworfen. Diese habe ich bewusste eingebaut, den meine Login-Page zeigt oben Fehlermeldungen ein, falls die Anmeldung nicht erfolgreich war. Bei falschen Zugangsdaten wirft mein Authentifizierer eine `BadCredentialsException`. Wichtig ist, dass alle geworfenen Exceptiones Subklassen von `AuthenticationException` sind, sonst kann durch folgenden Thymeleaf-Anweisung in der Login-Page nicht der korrekte Fehler ausgeben werden:

```html
<div class="error" th:if="${param.error}">
    <div class="alert alert-danger" th:with="errorMsg=${session['SPRING_SECURITY_LAST_EXCEPTION'].message}">
        <span th:text="${errorMsg}"></span>
    </div>
</div>
```

*Auszug aus login.html*

In Aktion sehen die Fehler so aus:

![invalid credentials](img/DataServer_Err1.PNG)

![invalid credentials](img/DataServer_Err2.PNG)

Der nächste Schritt war das Abfragen von Windpark Daten. 

Ich übernahm mein MongoRepository `WindparkDataRepository` vom letzten Beispiel. Mein Ziel war es, wie im Beispiel, per Thymeleaf Daten schön auszugeben. Konkrete möchte ich die Möglichkeit geben, alle Daten anzugeigen bzw. nach Windpark oder Windpark und Windengine zu filtern.

Zuerst brauchte ich funktionierende Queries für Mongo, um sie in Spring im Repository zu implementieren. Schnell wurde meine fragwürdige Datenstruktur zum Verhängnis. Meine Datenstruktur besteht aus einer Map bzw. Dictionary, als Key wird immer eine WindparkID genommen und der jeweilige Value ist ein Array mit den Daten der Windräder. Um aus diesen verschachtelten Datensatz Windengine-Daten pro Windpark raus zu filtern braucht es folgende Abfrage:

```sql
db.windparkData.aggregate(
	[{"$match": {"$and": [{"windparkID": "0"}, {"data": {"$elemMatch": {"windengineID": "0"}}}]}},
     { "$unwind": "$data"},
     { "$match": {"data.windengineID": "0"}}]
)
```

So eine Aggregatsfunktion lässt sich mit einen normalen MongoRepository nicht nachbilden. Ich musste ein zusätzliche Klasse definieren (`WindparkDataRepositoryImpl`), die zusätzliche Queries beinhaltet. 

Wenn man nicht mit einem MongoRepository arbeitet, bietet die Klasse `MongoTemplate` die Möglichkeit Daten aus einer Mongo-Datenbank zu bekommen. Doch um ehrlich zu sein finde ich die Syntax recht komisch, anstatt einfach die BSON-Notation als String zu benutzen, muss man folgendes bauen um die oberer Abfrage nachzubilden:

```java
Aggregation agg = newAggregation(
    Aggregation.match(new Criteria().andOperator(Criteria.where("windparkID").is(windparkID),
                                                 Criteria.where("data").elemMatch(Criteria.where("windengineID").is(windengineID)))),
    Aggregation.unwind("data"),
    Aggregation.match(Criteria.where("data.windengineID").is("0")));

AggregationResults<WindengineData> results =
                mongoTemplate.aggregate(agg, "windparkData", WindengineData.class);
```

Was aber ein noch größeres Problem darstellte war, dass die erhaltenen Daten komplett falsch waren. Man kann mithilfe der Methode `getMappedResults()` die Daten angeblich gemappt als Java-Objekt erhalten, nur leider werden alle Attribute auf Null bzw. Default-Werte gesetzt. Wieso dieser Fehler zustande kommt weiß ich nicht, doch ich baute mir eine Lösung. Ich bemerkte, dass die "rohen" Daten aus der Datenbank korrekt waren. Somit nehme ich diese, iteriere durch sie durch bis ich zu Windengine-Daten komme und parse sie erst an dieser Stelle ich ein Java-Objekt um. Für das Parsen fand ich eine tolle API namens Gson, mit dieser kann man aus einem Json-String ein Java-Objekt erstellen. Dieser Ansatz ist alles andere als schön, er funktioniert aber:

```java
Document allDocuments = results.getRawResults();
ArrayList<Document> rawData = (ArrayList<Document>) allDocuments.get("result");

// Handle conversion JSON -> WindengineData
Gson gson = new Gson();

ArrayList<WindengineData> resultSet = new ArrayList<>();

for (Document data : rawData) {
	// Get WindengineData as BSON
	Document rawOne = (Document) data.get("data");
	// Convert BSON -> JSON -> Java Object
	// Add result
	resultSet.add(gson.fromJson(rawOne.toJson(), WindengineData.class));
}
Collections.sort(resultSet);
return resultSet;
```

Zur Webanzeige nahm ich mir die Klassen `WebController` und `SearchCriteria` aus der Microservice-Demo zur Hilfe. `SearchCriteria` macht nichts anderes als zu schauen, ob der User auf der Suchseite die korrekten Parameter eingegeben hat. Falls ja, wird im  `WebController` die Methode `doSearch` aufgerufen, wenn der User den Find-Button drückt. In der Methode wird geschaut welche Parameter eingeben wurden, um die dazugehörige Methode aufzurufen, die die korrekte html-Seite zurückgibt. 

Falls ein User alle Daten ansehen möchte, wird die Methode `windzentraleSearch` aufgerufen:

```java
@RequestMapping("/data/windzentrale/verbose")
public String windzentraleSearch(Model model) {
    logger.info("Data-Service windzentraleSearch() invoked");
    WindzentraleData data = repository.findAllData();
    logger.info("Data-Service windzentraleSearch() found " + data.size() + " data records");
    model.addAttribute("windparkID", "0");
    model.addAttribute("windengineID", "all");
    model.addAttribute("type", "windzentrale");
    if (data != null)
    	model.addAttribute("data", data);
    return "datav";
}
```

*Aus `WebController` - Data Service*

Was passiert hier? Zuerst werden Daten aus der Datenbank per `repository.findAllData()` geholt. Danach werden dieser per `model.addAttribute("data", data)` durch Spring an Thymeleaf weitergeben, dass die Seite parst und in ihrer  schlussendlichen Form zurückgibt.

Da Füllen der Daten erfolgt also per Thymeleaf. Dies sieht im html-File so aus:

```html
<div th:each="park : ${data.zentrale}">
    <tr th:each="record : ${park.value}">
        <td th:text="${record.timestamp}"></td>
        <td th:text="${park.key}"></td>
        <td th:text="${record.windengineID}"></td>
        <td th:text="${record.windspeed}"></td>
        <td th:text="${record.rotationspeed}"></td>
        <td th:text="${record.bladeposition}"></td>
        <td th:text="${record.temperature}"></td>
        <td th:text="${record.power}"></td>
        <td th:text="${record.blindpower}"></td>
    </tr>
</div>
```

*Auszug aus datav.html*

Das `th:each` ist vergleichbar mit einer for-each Schleife. Wie in Java kann man mit `.key` oder `.value` auf eine HashMap zugreifen.

Schlussendlich baute ich noch eine html-Seite für die Suche:

![Data Search](img/DataServer_Search.PNG)

Gibt man nichts in die Felder ein, bekommt man alle Daten spendiert:

![Data Search all](img/DataServer_Search1.PNG)

Gibt man eine WindparkID ein, bekommt alle Daten aus diesem Windpark:

![Data Search](img/DataServer_Search2.PNG)

Wenn man WindparkID und WindengineID eingibt, bekommt man nur die Daten dieser Windkraftanlage auf dem gewählten Windpark:

![Data Search](img/DataServer_Search3.PNG)

## Fragestellungen

- Was versteht man unter Microservices?   
  Unter einem Microservice versteht man eine Architektur, bei der eine Anwendung durch eine Anzahl kleinerer Services realisiert wird. Jede Funktion wird somit durch einen separate Services realisiert. Jeder Service läuft auf einem eigenen Prozess, Kommunikation untereinander erfolgt oft durch leichtgewichtige Webschnittstellen.    
  Das Gegenstück zu Microservices ist eine monolithische Architektur. Bei dieser sind alle Funktionen in einer Anwendung gekapselt. Dies hat den Nachteil, dass bei einer kleinen Änderung die gesamte Applikation neu gebuilded und deployed werden muss. Außerdem muss bei einer Lastaufteilung die gesamte Applikation immer benutzt werden, bei Microservices dagegen kann ich einzelne Service mal mehr oder weniger Lastverteilen. Somit kann ich bei Microservices einzeln bestimmen, wie stark jede Funktion, also Service lastverteilt werden muss.

  

- Stellen Sie anhand eines Beispiels den Einsatz von Microservices dar.   
  Microservices haben einen Sinn bei sehr großen Projekten. Bei jeder kleinsten Änderung muss alles neu kompiliert, gebuilded, etc. werden. Ich las einen Artikel über Amazon und möchte ihn auch als Beispiel verwenden. Um die 2000er Jahre basierte die Amazon-Seite noch auf einer monolithischen Architektur, alles geschah durch eine Applikation. Mit der Zeit wurde es immer schwieriger neue Funktionen zu implementieren, weil der Overhead einfach zu groß war. Also beschloss man Funktionen in eigene Services zu separieren. Als Beispiel wurden genannt ein Service der den "Buy"-Knopf rendert oder ein Service der die Steuern berechnet. Mit dieser strengen Gliederung konnte man besser neue Funktionen eingliedern. Wenn man heute Amazon anschaut, dann gibt es verschiedene Seiten wie den Shop, den Video- bzw Musikstreaming Dienst und vieles mehr. Das sind wahrscheinlich Services, die in weitere Services untergliedert sind. Somit kann man sagen, dass ohne Microservices nie so ein Amazon entstanden wäre, wie wir es heute kennen.

  

- Wie kann man Spring Cloud nutzen und welche Tools werden dabei unterstützt?    
  Spring Cloud kann für diverse Anwendungen in Bezug auf verteilte Systeme benutzt werden, wie z.B. Microservices. Unter "Tools" verstehe ich die verschiedenen Cloud-Projekte die man benutzen kann. Ein Beispiele dafür wäre Spring Cloud Netflix, um Komponenten wie den Registration-Server Eureka benutzen zu können. Es gibt aber noch andere Projekte wie Spring Cloud Stream oder Spring Cloud AWS die man benutzen kann. Die gesamt Liste, verlinkt mit Quick-Starts und Dokumentation kann man [hier](https://spring.io/projects/spring-cloud#overview) finden.    
  Um ein Spring-Projekt bequem zu erstellen, kann man den [Spring Initalizr](https://start.spring.io/) verwenden. In diesem kann man auch Cloud-Dependencies hinzufügen.

  

- Beschreiben Sie das Spring Cloud Netflix Projekt. Aus welchen Bestandteilen setzt sich dieses Projekt zusammen?   
  Früher basierten die Netflix-Systeme auf einer monolithischen Architektur. Doch als der steigende User-Traffic langsam nicht mehr zu bewältigen war, baute Netflix den Dienst zwischen 2008 und 2012 zu einem Microservice um. Teile dieser Software werden als Open-Source zur Verfügung stellt. Ein Teil dieser Initiative ist *Spring Cloud Netflix*. Dieses gliedert sich in sechs Dienste auf:

  - Eureka: Service-Registry und Mid-tier Loadbalancer

  - Hystrix: Bibliothek, die Fehler beim Zugriff auf externe Dienste isoliert und so die Ausfallsicherheit in einem verteilten System erhöht ([Circuit Breaker](http://martinfowler.com/bliki/CircuitBreaker.html))

  - Ribbon: Library für Interprozesskommunikation mit eingebautem Softwarelastverteiler

  - Archaius: Bibliothek für Konfigurationsmanagement, basiert auf Apache Commons Configuration und erweitert das Projekt für den Cloud-Einsatz

  - Zuul: Edge-Service für dynamisches Routing, Monitoring, Security, Ausfallsicherheit und einiges andere mehr

  - Feign: Library, die das Schreiben von HTTP-Clients vereinfacht

    

- Wofür werden die Annotations @EnableEurekaServer und @EnableDiscoveryClient verwendet?

  - @EnableEurekaServer     
    Wird benötigt, um einen Service als Eureka Server zu markieren, der als Discovery Service fungieren soll.

  - @EnableDiscoveryClient   
    Wird benötigt, damit sich der Service am Registration-Server registrieren und von ihm auch gefunden werden kann. Damit dies vollständig funktioniert, wird wenigsten eine minimale Konfiguration gemacht werden. Die kann wie folgt aussehen:

    ```yaml
    # Spring properties
    spring:
      application:
         name: accounts-service
    
    # Discovery Server Access
    eureka:
      client:
        serviceUrl:
          defaultZone: http://localhost:1111/eureka/
    
    # HTTP Server
    server:
      port: 2222   # HTTP (Tomcat) port
    ```

    

- Wie werden in dem Account Service die Properties gesetzt und welche Parameter werden hier verwendet?

  Wie im Punkt vorher angesprochen, werden die Properties über ein yaml-File gesetzt. Damit dieses gefunden werden kann, muss es als Property gesetzt werden:

  ```java
  public static void main(String[] args) {
  	// Tell server to look for accounts-server.properties or
  	// accounts-server.yml
  	System.setProperty("spring.config.name", "accounts-server");
  
  	SpringApplication.run(AccountsServer.class, args);
  }
  ```

  Als "Haupt"-Parameter werden `spring` und `eureka` benutzt. Über ersteres erfolgt die grundlegende Spring-Konfiguration, die man eher aus `application.properties` kennt. Über `eureka` werden die Discovery-Server Eigenschaften gesetzt, wie die Discovery-Server Adresse über `eureka.client.serviceUrl.defaultZone` oder wie lange die Registrierung dauern soll über `eureka.instance.leaseRenewalIntervalInSeconds`.

  

- Wie funktioniert das Logging bei diesem Beispiel? Ist es möglich das Logging zu erhöhen bzw. komplett abzudrehen?
  Wenn ja, wie?    
  Spring Boot setzt default-mäßig das Logging auf Level *INFO*. Dies kann man aber durch das Config-File ändern. Dafür müsste man aber im Beispiel drei Config-Files ändern. Um dies zu vereinfachen kann man dieses Verhalten in einem File namens *logback.xml* setzen. Zumindest Theoretisch.   
  Ich habe versucht die Logger auszuschalten, geschafft habe ich es nicht.   

  ```xml
  <!-- Specify logging levels -->
  <logger name="org.springframework" level="off"/>
  <logger name="org.hibernate" level="off"/>
  ```

  Beim Ausführen aber, wird das Loggen weiter ausgeführt. Ich dachte, dass es vielleicht an der IDE liegt, also führte ich es im Terminal aus. Loggen funktioniert weiter. Ich versuchte manuell das Config-File als Parameter zu sitzen:

  ```
  java -jar -D"logging.config"="file:logback.xml" target/microservices-demo-2.0.0.RELEASE.jar web
  ```

  Loggen ging immer noch. Ich probierte weiter und änderte das Logging Level im yaml-File:

  ```yaml
  logging:
    level:
      org.springframework.web: OFF
      guru.springframework.controllers: OFF
      org.hibernate: OFF
  ```

  Dies funktionierte immer noch nicht. Ich weiß nicht was ich falsch gemacht habe, aber wieso es nicht funktioniert kann ich mir nicht erklären.

## Quellen

* [Spring Microservice Introduction](<https://spring.io/blog/2015/07/14/microservices-with-spring>)
* [MongoDB - @Query](<https://www.mkyong.com/spring-boot/spring-boot-spring-data-mongodb-example/>)
* [Spring Security - Don´t block REST](<https://stackoverflow.com/questions/34957073/spring-security-blocking-rest-controller>)
* [Spring REST - Boolean per POST zurückgeben](<https://stackoverflow.com/questions/33458206/how-to-return-a-boolean-value-from-a-spring-mvc-controller-to-ajax?rq=1>)
* [Spring Security - "Custom Exceptions"](<https://stackoverflow.com/questions/12035133/use-custom-exceptions-in-spring-security>)
* [Custom Login Page via Bootstrap](https://memorynotfound.com/spring-boot-spring-security-thymeleaf-form-login-example/)
* [Thymeleaf - Exceptions anzeigen](https://stackoverflow.com/questions/30295343/spring-security-thymeleaf-lockedexception-custom-message)
* [Convert JSON to Java-Object - Gson](https://www.quora.com/What-are-the-ways-of-converting-a-Java-object-to-a-MongoDB-document-and-vice-versa)
* [MongoRepository erweitern](https://www.mkyong.com/spring-data/spring-data-add-custom-method-to-repository/)
* [Thymeleaf - Map iterieren](https://stackoverflow.com/questions/42966653/print-key-value-in-thymeleaf-from-hashmap-arraylist)
* [Thymeleaf - map.get(key)](https://stackoverflow.com/questions/28621301/how-to-use-map-getkey-in-thymeleaf-broadleaf-ecom)
* [Microservices](https://martinfowler.com/articles/microservices.html)
* [What Led Amazon to its Own Microservices Architecture](https://thenewstack.io/led-amazon-microservices-architecture/)
* [Spring Cloud Netflix](https://www.heise.de/developer/artikel/Eureka-Microservice-Registry-mit-Spring-Cloud-2848238.html?seite=all)