package Windanlage.windengine;


import Windanlage.model.WindengineData;
import org.springframework.stereotype.Service;

import Windanlage.model.WindengineData;

@Service
public class WindengineService {
	
	public String getGreetings( String inModule ) {
        return "Greetings from " + inModule;
    }

    public WindengineData getWindengineData(String inWindengineID ) {
    	
    	WindengineSimulation simulation = new WindengineSimulation();
        return simulation.getData( inWindengineID );
        
    }
    

    
}