@echo off
echo Starting ApacheMQ
start C:\Programme_etc\apache-activemq-5.15.3\bin\activemq start

echo Starting Windengines
cd Windanlage

echo Starting Engine 0 at 8090 in Windpark 0
start mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=8090,--windengine.id=0,--windpark.id=0

echo Starting Engine 1 at 8091 in Windpark 0
start mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=8091,--windengine.id=1,--windpark.id=0

echo Starting Engine 2 at 8092 in Windpark 0
start mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=8092,--windengine.id=2,--windpark.id=0

echo Starting Engine 0 at 8190 in Windpark 1
start mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=8190,--windengine.id=0,--windpark.id=1

echo Starting Engine 1 at 8191 in Windpark 1
start mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=8191,--windengine.id=1,--windpark.id=1

echo Starting Engine 2 at 8192 in Windpark 1
start mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=8192,--windengine.id=2,--windpark.id=1
cd ..

echo Starting Windparks
cd Windpark

echo Starting Windpark 0 at 8080
start mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=8080,--windpark.id=0

echo Starting Windpark 0 at 8180
start mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=8180,--windpark.id=1
cd ..

;echo Starting Windzentrale at 9999
;cd Windzentrale
;start mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=9999,--windpark.count=2

